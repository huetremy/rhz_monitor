# Simple Network Monitor

## Introduction

L'objectif de ce repo est de fournir un outil simple de monitoring d'un réseau, basé sur la découverte de celui-ci.

L'outil permet, via un serveur [gotify](https://gotify.net/) de créer des notifications push en cas de problème sur le réseau.

