#!/usr/bin/python3

""" Simple network monitor tool

The aim of this project is to provide a simple network monitor tool
This script reads a list of IPs that should be present on a network, and throws a notification on a gotify server if one is missing

There is no need to install anything on the equipments that are monitored, they just need to answer ping

This script also provide a tool to generate a list of equipments using an arp request to discover them

LICENSE : "THE BEER-WARE LICENSE" (Revision 42):
<remy.huet@etu.utc.fr> wrote this file.  As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
Rémy Huet


"""

import argparse
import json
import os
import subprocess
import requests

from time import sleep


""" Discover network

This function is called when the script is invoked with the --discover flag.
It runs a nmap -sP -n -T4 --min-parallelism 100 on every subnet defined in the config file.

Then, the user is asked to give a description for every IP found.
The results are written in the list file.
The older file is marked as .old

"""

def discover_network ():
    print ("Discovering network (this may take a while)...")
    ips = ""
    for subnet in config["autoscan_subnets"] :
        print ("    Scanning " + subnet + "\n") if args.verbose else None
        ips_subnet = subprocess.check_output("nmap -sP -n -T4 --min-parallelism 100 " + subnet + " | grep report | awk '{print $5}'", shell=True).decode()
        print ("        IPs found in subnet : \n" + ips_subnet) if args.verbose else None
        ips += ips_subnet

    hosts = list()

    for ip in ips.splitlines():
        print ("Ip " + ip + " was detected. Give it a description :")
        desc = str(input())
        hosts.append({"ip": ip, "description": desc})

    print ("Data :" + str(hosts)) if args.verbose else None

    print ("Moving " + args.list_file + " to " + args.list_file + ".old \n") if args.verbose else None
    os.rename (args.list_file, args.list_file + ".old")

    with open (args.list_file, "w") as list_file:
        print ("Writing to " + args.list_file + "\n") if args.verbose else None
        json.dump(hosts, list_file)

""" Send notification

Send a notification with given title and description to gotify server.
"""
def send_gotify_message(title, description):
    uri = config["gotify_uri"] + "/message?token=" + config["gotify_token"]
    print ("Sending notification") if args.verbose else None
    r = requests.post(uri, data={"title": title, "message": description, 'priority': 10})
    print (str(r.status_code) + str(r.content)) if args.verbose else None

""" Check network

This function is called when the script is invoked without the --discover flag.
It check the presence of the hosts listed in the list file, using a ping -c 3 -W 4

If a host listed in the list file does not answer the three pings, it will throw a notification using the Gotify server provided in config file

"""
def check_network ():
    print ("Checking network...") if args.verbose else None
    with open (args.list_file) as list_file:
        hosts = json.load(list_file)

    for host in hosts:
        print ("    Checking for " + host["description"]) if args.verbose else None
        try:
            subprocess.check_call("ping -c 3 -W 4 " + host["ip"], shell=True, stdout=FNULL)
            print("         FOUND")
        except subprocess.CalledProcessError:
            print (host["description"] + " not found \n") if args.verbose else None
            title = host["description"] + " down"
            description = "Host " + host["description"] + " (" + host["ip"] + ") does not answer to ping anymore"
            send_gotify_message(title, description)

    print('Network check finished')



if __name__ == '__main__':
    # Create argument parser
    parser = argparse.ArgumentParser(description="Simple network monitor tool")

    # Add different arguments
    parser.add_argument('--config-file', '-c', action='store', dest='config_file', default='config.json', help='Config file path (default ./config.json)')
    parser.add_argument('--discover', '-d', action='store_true', default=False, help='Discover networks using "autoscan_subnets" config array')
    parser.add_argument('--list-file', '-l', action='store', dest='list_file', default='list.json', help='List of IPs file path (default ./list.json)')
    parser.add_argument('--single-run', '-s', action='store_true', dest='single', default=False, help='Run only one time instead of starting the loop')
    parser.add_argument('--time', '-t', action='store', dest='time', default=60, type=int, help='Interval between two scans in seconds (default 60)')
    parser.add_argument('--test-notification', '-n', action='store_true', dest='test_notif', default=False, help='Send a test notification')
    parser.add_argument('--verbose', action='store_true', default=False, help='Make output verbose')
    parser.add_argument('--version', '-v', action='version', version='%(prog)s 2.1.0', help='Print program version')

    args = parser.parse_args()
    with open(args.config_file) as config_file:
        config = json.load(config_file)
    FNULL=open ("/dev/null", "w")

    # If --test-notif flag is set, send a test notification to the gotify server then exit
    if args.test_notif: 
        send_gotify_message('Test notification', 'Lorem ipsum dolor sit amet')
        exit()

    # If --discover flag is set, discover subnets then exit
    if args.discover : 
        discover_network()
        exit()

    # If --single-run is set, run the check one time then exit
    if args.single: 
        check_network()
        exit()
    
    # Inconditionnaly check with a period of --time seconds (60 by default)
    while True:
        check_network()
        sleep(args.time)
